# Text Editing Library

A simple string with utilities for editing, specifically designed to work with non-ASCII strings.

## Features

- Provides a `TextLine` struct that represents an editable text line.
- Supports efficient insertion, removal, and manipulation of characters and ranges.
- Handles non-ASCII characters correctly, taking into account their multi-byte nature.
- Offers convenient methods for moving the text cursor forward, backward, and skipping over words.
- Implements the `Display` trait for easy conversion to a string.

## Documentation

API documentation can be found at [docs.rs/text-editing](https://docs.rs/text-editing).

## Example

```Rust
use text_editing::TextLine;

let mut line = TextLine::from_string("Hello, 🌍!".into());
line.insert(7, 'w');
assert_eq!(line.as_str(), "Hello, w🌍!");

let removed_char = line.remove(7);
assert_eq!(removed_char, 'w');
assert_eq!(line.as_str(), "Hello, 🌍!");
```
