use super::TextLine;

impl TextLine {
    /// Adds a char at the current text cursor position.
    ///
    /// # Arguments
    /// * `text_cursor` - A mutable reference to the current text cursor position.
    /// * `c` - The char to add.
    ///
    /// # Examples
    /// ```
    /// use text_editing::TextLine;
    ///
    /// let mut line = TextLine::from_string("Hello, ".into());
    /// let mut text_cursor = 7;
    /// line.add_char(&mut text_cursor, 'w');
    /// assert_eq!(line.as_str(), "Hello, w");
    /// assert_eq!(text_cursor, 8);
    /// ```
    pub fn add_char(&mut self, text_cursor: &mut usize, c: char) {
        self.insert(*text_cursor, c);
        *text_cursor += 1;
    }

    /// Removes the next char at the current text cursor position, if it exists.
    ///
    /// # Arguments
    /// * `text_cursor` - The current text cursor position.
    ///
    /// # Returns
    /// `true` if a char was removed, `false` otherwise.
    ///
    /// # Examples
    /// ```
    /// use text_editing::TextLine;
    ///
    /// let mut line = TextLine::from_string("Hello, world!".into());
    /// let text_cursor = 7;
    /// assert!(line.remove_forward(text_cursor));
    /// assert_eq!(line.as_str(), "Hello, orld!");
    /// ```
    pub fn remove_forward(&mut self, text_cursor: usize) -> bool {
        let remove = text_cursor < self.len();
        if remove {
            self.remove(text_cursor);
        }
        remove
    }

    /// Removes the next word at the current text cursor position, if it exists.
    ///
    /// # Arguments
    /// * `text_cursor` - The current text cursor position.
    ///
    /// # Returns
    /// `true` if a word was removed, `false` otherwise.
    ///
    /// # Examples
    /// ```
    /// use text_editing::TextLine;
    ///
    /// let mut line = TextLine::from_string("Hello, world!".into());
    /// let text_cursor = 7;
    /// assert!(line.remove_forward_skip(text_cursor));
    /// assert_eq!(line.as_str(), "Hello, !");
    /// ```
    pub fn remove_forward_skip(&mut self, text_cursor: usize) -> bool {
        let mut end_cursor = text_cursor;
        self.skip_forward(&mut end_cursor);
        let range = text_cursor..end_cursor;
        let moved = !range.is_empty();
        if moved {
            self.remove_range(range);
        }
        moved
    }

    /// Removes the previous char at the current text cursor position, if it exists, and updates the text cursor.
    ///
    /// # Arguments
    /// * `text_cursor` - A mutable reference to the current text cursor position.
    ///
    /// # Returns
    /// `true` if a char was removed, `false` otherwise.
    ///
    /// # Examples
    /// ```
    /// use text_editing::TextLine;
    ///
    /// let mut line = TextLine::from_string("Hello, world!".into());
    /// let mut text_cursor = 7;
    /// assert!(line.remove_backward(&mut text_cursor));
    /// assert_eq!(line.as_str(), "Hello,world!");
    /// assert_eq!(text_cursor, 6);
    /// ```
    pub fn remove_backward(&mut self, text_cursor: &mut usize) -> bool {
        let remove = *text_cursor > 0;
        if remove {
            *text_cursor -= 1;
            self.remove(*text_cursor);
        }
        remove
    }

    /// Removes the previous word at the current text cursor position, if it exists, and updates the text cursor.
    ///
    /// # Arguments
    /// * `text_cursor` - A mutable reference to the current text cursor position.
    ///
    /// # Returns
    /// `true` if a word was removed, `false` otherwise.
    ///
    /// # Examples
    /// ```
    /// use text_editing::TextLine;
    ///
    /// let mut line = TextLine::from_string("Hello, world!".into());
    /// let mut text_cursor = 12;
    /// assert!(line.remove_backward_skip(&mut text_cursor));
    /// assert_eq!(line.as_str(), "Hello, !");
    /// assert_eq!(text_cursor, 7);
    /// ```
    pub fn remove_backward_skip(&mut self, text_cursor: &mut usize) -> bool {
        let end_cursor = *text_cursor;
        self.skip_backward(text_cursor);
        let range = *text_cursor..end_cursor;
        let moved = !range.is_empty();
        if moved {
            self.remove_range(range);
        }
        moved
    }
}
